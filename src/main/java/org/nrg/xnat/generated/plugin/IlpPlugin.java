package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_webapp_ilp", name = "XNAT 1.7 ILP Plugin", description = "This is the XNAT 1.7 ILP Plugin.")
public class IlpPlugin {
}